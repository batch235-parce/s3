const express = require("express");
const app = express();
const port = 4000;

app.use(express.json());

let users = [
	{
		name: "Jojo Joestar",
		age: 25,
		username: "Jojo"
	},
	{
		name: "Dio Brando",
		age: 23,
		username: "Dio"
	},
	{
		name: "Jotaro Kujo",
		age: 28,
		username: "Jotaro"
	}
]

// [SECTION] Routes and Controllers for Users
app.get("/users", (req, res) => {
	return res.send(users);
})

// add simple if statement that if the request body does not have property name, we will send message along with a 400 http status code (Bad Request)
// hasOwnProperty() return a boolean if the property name passed exists or does not exist in a given object.
app.post("/users", (req, res) => {
	if(!req.body.hasOwnProperty("name")){
		return res.status(400).send({
			error: "Band Request - missing required parameter NAME"
		})
	}
	if(!req.body.hasOwnProperty("age")){
		return res.status(400).send({
			error: "Band Request - missing required parameter AGE"
		})
	}
})

// ACTIVITY
let artists = [
	{
		name: "Taylor Swift",
		songs: ["Stay Stay Stay", "I Almost Do"],
		album: "Red (Taylor's Version)",
		isActive: true
	},
	{
		name: "BLACKPINK",
		songs: ["Pink Venom", "Shutdown"],
		album: "Born Pink",
		isActive: true
	},
	{
		name: "Sasha Sloan",
		songs: ["Is It Just Me", "Thank God"],
		album: "Only Child",
		isActive: true
	}
]


app.get("/artists", (req, res) => {
	return res.send(artists);
})

app.post("/artists", (req, res) => {
	if(!req.body.hasOwnProperty("name")){
		return res.status(400).send({
			error: "Band Request - missing required parameter NAME"
		})
	}

	if(!req.body.hasOwnProperty("songs")){
		return res.status(400).send({
			error: "Band Request - missing required parameter SONGS"
		})
	}

	if(!req.body.hasOwnProperty("album")){
		return res.status(400).send({
			error: "Band Request - missing required parameter ALBUM"
		})
	}

	if(req.body.isActive === false){
		return res.status(400).send({
			error: "Band Request - required parameter isActive should be TRUE"
		})
	}
})

app.listen(port, () => console.log(`Server is runnig at port ${port}`))