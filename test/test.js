const chai = require("chai");
const { assert } = require("chai");

// npm run-script specifictest "test suite string message" - running a specific test suites
// important and use chai-http to allow chai to send requests to our server

const http = require("chai-http");
chai.use(http);

describe("API Test Suite for users", () => {
	it("Test API get users is running", (done) => {
		// requests() method is used from chai to create an http request given to the server
		// get("/endpoint") method is used to run/access a get method route
		// end() method is used to acces the response from the route. It has anonymous function as an argument that receives 2 objects,
		// 		the err or the response
		chai.request("http://localhost:4000")
		.get("/users")
		// res.body contains the body of the response. The data sent from res.send()
		// isArray() is an assertion that the given data is an array
		.end((err, res) => {
			// isDefined is assertion that given data is not undefined. It's like a shortcut to ...
			assert.isDefined(res);
			done();
		})
	})

	it("Test API get users returns an array", (done) => {
		chai.request("http://localhost:4000")
		.get("/users")
		.end((err, res) => {
			assert.isArray(res.body);
			done();
		})
	})

	it("Test API get users array first object username is Jojo", (done) => {
		chai.request("http://localhost:4000")
		.get("/users")
		.end((err, res) => {
			assert.equal(res.body[0].username, "Jojo");
			done();
		})
	})

	it("Test API get users array last item is not undefined", (done) => {
		chai.request("http://localhost:4000")
		.get("/users")
		.end((err, res) => {
			assert.notEqual(res.body.length-1, undefined);
			done();
		})
	})

	it("Test API post users returns 400 if no name", (done) => {
		chai.request("http://localhost:4000")
		.post("/users") // post() which is used by chai-http to access a post method
		.type("json") // type() which is used to tell chai that request body is going to be stringified as a json
		.send({
			age: 30,
			username: "jin92"
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})

	it("Test API post users returns 400 if no age", (done) => {
		chai.request("http://localhost:4000")
		.post("/users") 
		.type("json") 
		.send({
			name: "Lisa",
			username: "lalalisa_m"
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})
})


// ACTIVITY
describe("API Test Suite for artists", () => {
	it("Test API get artists is running", (done) => {
		chai.request("http://localhost:4000")
		.get("/artists")
		.end((err, res) => {
			assert.isDefined(res);
			done();
		})
	})

	it("Test API get songs property of the first object is an array", (done) => {
		chai.request("http://localhost:4000")
		.get("/artists")
		.end((err, res) => {
			assert.isArray(res.body[0].songs);
			done();
		})
	})

	it("Test API post songs returns 400 if there is no name", (done) => {
		chai.request("http://localhost:4000")
		.post("/artists") 
		.type("json") 
		.send({
			songs: ["Hell Nos And Headphones", "Rock Bottom"],
			album: "Haiz",
			isActive: true
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})

	it("Test API post songs returns 400 if there is no songs", (done) => {
		chai.request("http://localhost:4000")
		.post("/artists") 
		.type("json") 
		.send({
			name: "Hailee Steinfeld",
			album: "Haiz",
			isActive: true
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})

	it("Test API post songs returns 400 if there is no album", (done) => {
		chai.request("http://localhost:4000")
		.post("/artists") 
		.type("json") 
		.send({
			name: "Hailee Steinfeld",
			songs: ["Hell Nos And Headphones", "Rock Bottom"],
			isActive: true
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})

	it("Test API post songs returns 400 if isActive property is false", (done) => {
		chai.request("http://localhost:4000")
		.post("/artists") 
		.type("json") 
		.send({
			name: "Hailee Steinfeld",
			songs: ["Hell Nos And Headphones", "Rock Bottom"],
			album: "Haiz",
			isActive: false
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})


})